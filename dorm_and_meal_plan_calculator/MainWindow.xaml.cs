﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

// Group :- 4 | Assignment :- 9 | Dormitory and Meal Plan Calculator
namespace dorm_and_meal_plan_calculator
{
    /// Interaction logic for MainWindow.xaml
    
    public partial class MainWindow : Window
    {
        //Initilizing ViewModel and TotalCost class's objects.
        TotalCost tc = null;
        VM vm = new VM();
        public MainWindow()
        {
            InitializeComponent();
            DataContext = vm;
        }
        
        //This method calculates the total amount from the listboxes.
        private void btnCalculateTotalCost_Click(object sender, RoutedEventArgs e)
        {
            vm.Calculate();
            if (tc == null)
            {
                tc = new TotalCost(vm)
                {
                    Owner = this,
                    WindowStartupLocation = WindowStartupLocation.CenterOwner
                };
                tc.Closed += TC_Closed;
                tc.Show();
            }
        }

        //This method resets the listbox choices.
        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            btnClear.IsEnabled = false;
            btnCalculateTotalCost.IsEnabled = false;
            vm.Clear();
        }

        //To close the TotalCost Window.
        private void TC_Closed(object sender, System.EventArgs e) => tc = null;
        
        //This enables the button when user selects a field.
        private void listDorm_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            btnCalculateTotalCost.IsEnabled = true;
            btnClear.IsEnabled = true;
        }
    }
}
