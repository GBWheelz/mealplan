﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

// Group :- 4 | Assignment :- 9 | Dormitory and Meal Plan Calculator
namespace dorm_and_meal_plan_calculator
{

    public class VM : INotifyPropertyChanged
    {

        #region Initialization

        // Define properties used for interaction with XAML sheet!
        const decimal ALLEN_HALL_COST = 1500,
                      PIKE_HALL_COST = 1600,
                      FARTHING_HALL_COST = 1800,
                      UNIVERSITYSUITS_COST = 2500;

        const decimal SEVEN_MEAL_PER_WEEK_COST = 600,
                      FOURTEEN_MEAL_PER_MEAL_WEAK = 1200,
                      UNILIMITED_COST = 1700;
        
        public BindingList<Thing> DormList { get; set; } = new BindingList<Thing>()
        {
            new Thing("DormList", "Allen Hall ", ALLEN_HALL_COST),
            new Thing("DormList", "Pike Hall ", PIKE_HALL_COST),
            new Thing("DormList", "Farthing Hall ", FARTHING_HALL_COST),
            new Thing("DormList", "University Suits ", UNIVERSITYSUITS_COST)
        };

        public BindingList<Thing> MealList { get; set; } = new BindingList<Thing>()
        {
            new Thing("MealPlanList", "7 Meals Per Week ", SEVEN_MEAL_PER_WEEK_COST),
            new Thing("MealPlanList", "14 Meals Per Week ", FOURTEEN_MEAL_PER_MEAL_WEAK),
            new Thing("MealPlanList", "Unlimited Meals ", UNILIMITED_COST)
        };

        Thing selectedDorm = null;
        public Thing SelectedDorm
        {
            get { return selectedDorm; }
            set { selectedDorm = value; notifyChange(); }
        }

        Thing selectedMealPlan = null;
        public Thing SelectedMealPlan
        {
            get { return selectedMealPlan; }
            set { selectedMealPlan = value; notifyChange(); }
        }
        
        public decimal totalCost = 0;
        public decimal TotalCost
        {
            get { return totalCost; }
            set { totalCost = value; notifyChange(); }
        }

        public decimal dormCost;
        public decimal DormCost
        {
            get { return dormCost; }
            set { dormCost = value; notifyChange(); }
        }

        public decimal mealPlanCost;
        public decimal MealPlanCost
        {
            get { return mealPlanCost; }
            set { mealPlanCost = value; notifyChange(); }
        }
        #endregion

        #region Logic

        //This function calculates the total amount from the user selected choices.
        public void Calculate()
        {
            TotalCost = SelectedDorm.DormCost + SelectedMealPlan.MealPlanCost;
        }

        //To clear user's choices.
        public void Clear()
        {
            SelectedDorm = null;
            SelectedMealPlan = null;
        }
        #endregion

        #region PropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        private void notifyChange([CallerMemberName]string name = "") => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        #endregion
    }

}
