﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// Group :- 4 | Assignment :- 9 | Dormitory and Meal Plan Calculator
namespace dorm_and_meal_plan_calculator
{
    public class Thing
    {
        #region Initialization

        // Define properties used for interaction with XAML sheet!

        public string DormName { get; set; } = "";
        
        public string MealPlanName { get; set; } = "";

        public decimal DormCost { get; set; } = 0;

        public decimal MealPlanCost { get; set; } = 0;
        #endregion

        #region Logic 

        //This funtion is a constructor which loads the date into the listboxes.
        public Thing(string listName, string itemName, decimal cost)
        {
            if (listName == "DormList")
            {
                DormName = $"{itemName} - ${cost} Per Semester";
                DormCost = cost;
            }
            else if (listName == "MealPlanList")
            {
                MealPlanName = $"{itemName} - ${cost} Per Semester";
                MealPlanCost = cost;
            }
        }
        #endregion
    }
}
