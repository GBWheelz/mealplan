﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace dorm_and_meal_plan_calculator
{
    /// <summary>
    /// Interaction logic for TotalCost.xaml
    /// </summary>
    public partial class TotalCost : Window
    {
        //Initilizing ViewModel class's objects.
        VM vm = null;
        const String RESULT_STRING = "{0} + {1} and Total Cost = ${2} Per Semester\r\n\n";

        public TotalCost(VM vm)
        {
            this.vm = vm;
            InitializeComponent();
            DataContext = vm;
        }

        //This method saves the calculated date into the file!.
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            string output = string.Format(RESULT_STRING, lblDormCost.Content.ToString(), lblMealPlanCost.Content.ToString(), txtTotal.Text.ToString());
            File.AppendAllText("OutPut.Txt", output);
            btnSave.IsEnabled = false;
        }

        //To exit from the current window.
        private void btnExit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
