# MealPlan
-Update on January 14, 2019
-This application is a c#/visual studio application. 
-It is an example application created for a school project
-It requires Visual Studio to compile and run.
-The application will display two list boxes.  Select a dormitory location from the first list box 
-and the meal plan from the second list box.  Click the calculate button.
-A second window will open that will display the selected items along with their line item costs and the total.
-There is a compiled executable file in \mealplan\dorm_and_meal_plan_calculator\bin\Debug that will run the application.

-Update on January 15, 2019 - License Note
-Added a GNU GPL license for the project.  The text of the license came from the following location:
-https://opensource.org/licenses/GPL-2.0
-The opensource license was selected as it is a free license that can be used by anyone, and is easily available.
-The opensource license allows anyone to use, modify or distribute the code as long as the source is acknowledged.
-This software is not proprietary and does not require any protection, so an opensource license is reasonable.
-The license text can be found in the license.md file in the root of this project.

-Updated on January 15, 2019 10:40pm
-Added a Wiki entry comparing and contrasting public and private projects.
